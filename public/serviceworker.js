const CACHE_NAME = "VERSION-1"
const urlsToCache = ['index.html', 'ofline.html'];
const self = this;

//install SW
self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then((cache) => {
                console.log('opened')

                return cache.addAll(urlsToCache)
            })
    )
})



//listen for requests 
self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request)
            .then(() => {
                return fetch(event.request)
                    .catch(() => caches.match('ofline.html'))
            })
    )
})


//activate SW
const whitelist = []
whitelist.push(CACHE_NAME)

self.addEventListener('activate', (event) => {
    event.waitUntil(
        caches.keys().then((cacheNames) => Promise.all(
            cacheNames.map((cacheNames) => {
                if (!whitelist.includes(cacheNames)) {
                    return caches.delete(cacheNames)
                }
            })
        ))
    )
})